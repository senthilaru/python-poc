> This is a simple python web application, with 2 url mappings in it.

**Install Python 3**

    https://www.python.org/downloads/

**Install PIP**

    sudo easy_install pip

**How to run**

    python3 -m http.server

**Open browser and type**

    http://localhost:8000
    http://localhost:8000/mongo
    