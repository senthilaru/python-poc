import web

urls = (
  '/', 'index',
  '/mongo', 'mongo'
)

app = web.application(urls, globals())

render = web.template.render('templates/')

class index(object):
    def GET(self):
        greeting = "Hello World"
        return render.index(greeting = greeting)

class mongo(object):
    def GET(self):
        greeting = "Good NoSQL DB"
        return render.mongo(greeting = greeting)
if __name__ == "__main__":
    app.run()
